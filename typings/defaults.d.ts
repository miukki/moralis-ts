interface CurrencyValueDTO {
  value: number
  currency: string
}

interface CurrencyValue extends CurrencyValueDTO {}

interface AddressDTO {
  addressName: string
  line1: string
  line2: string | null
  suburb: string | null
  city: string
  region: string | null
  country: string
  postcode: string
  type: string
  deliveryNote: string | null
  isDefault: boolean
}

interface Address extends AddressDTO {}

interface MessageDTO {
  messageType: 'Error' | 'Information' | 'Discount'
  text: string
}

interface ErrorResponseDTO extends Error {
  code: number
  message: string
  isTargetUx: boolean
  inputErrors: InputErrorResponseDTO[]
}

interface InputErrorResponseDTO extends Error {
  name: string
  code: number
  message: string
}

interface InputErrorResponse extends InputErrorResponseDTO {}

interface ErrorResponse extends ErrorResponseDTO {}
