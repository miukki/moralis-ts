import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import { useMoralis } from "react-moralis";
import {Router} from 'react-router-dom'
import {history} from 'utils/router/history'

// import logo from './logo.svg';
import './App.css';

function App() {
  const { setUserData, Moralis, login, signup, authenticate, isAuthenticated, user, logout, isAuthenticating, isInitialized, isUnauthenticated } = useMoralis();

  const [username, setUsername] = useState("Batman");
  const [password, setPassword] = useState("bive[fvtnjdf");
  const [email, setEmail] = useState("miukki@gmail.com");
  const [wallet, setWallet] = useState("");


  const resetPassword = () => {
    Moralis.User.requestPasswordReset(email)
.then(() => {
  // Password reset request was sent successfully
}).catch((error: {code: any, message: any}) => {
  // Show the error message somewhere
  alert("Error: " + error.code + " " + error.message);
});
  }


  const setUserDataHandler =  () => {
    setUserData({
  username: "Batman",
  email,
  numberOfCats: 12,
});
  }

  return (
    <div className="App">
      <div className="App-content">

              {isAuthenticating ? <div>Loading...</div> : null}
        {isInitialized ? <div>Moralis is initialized!</div> : null}


{isAuthenticated ? null : <div>

<div>

  {/* // [Your form and inputs somewhere here... ] */}
<button onClick={() => signup(username, password, email)}>Sign up</button>
  {/* // [Your form and inputs somewhere here... ] */}
 <button onClick={() => login(username, password)}>Login (non-crypto)</button>

</div>



</div>}



        {  !isAuthenticated ?       <div>
        <button onClick={() => authenticate()}>Authenticate with Metamask</button>
        </div>
          :    <div>
      <h1>Welcome {user?.get("username")}</h1>

      <div>{JSON.stringify(Moralis?.User?.current())}</div>

        <button onClick={() => logout()}>Logout</button>
        <button onClick={() => setUserDataHandler()}>setUserData</button>

        
        <button onClick={() => resetPassword()}>Reset Password</button>


    </div>}
        
      </div>
    </div>
  );
}

export default App;
